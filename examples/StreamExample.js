import {bodyTemplate, headTemplate} from '../templates/chunkedTemplate.js';
import http from 'http';

http.createServer(async (req, res) => {
  res.writeHead(200, {
    'Content-Type': 'text/html',
    'Transfer-Encoding': 'chunked'
  })
  res.write('');
  res.write(headTemplate());
  await new Promise((r) => setTimeout(r, 4000));
  res.end(bodyTemplate());
}).listen(3000);
