import http from 'http';
import {mainTemplate} from '../templates/mainTemplate.js';

http.createServer(async (req, res) => {
  await new Promise((r) => setTimeout(r, 4000));
  res.writeHead(200, {
    'Content-Type': 'text/html'
  })
  res.write(mainTemplate());
  res.end();
}).listen(4000);
